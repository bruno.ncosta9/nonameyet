"use strict";

const settings =
{
   engine:{
      gravity: 0.5,
      ground: {
         y: 0
      }
   },
   sky: {
      image: "Assets/img/sky.png",
      width: 1000,
      height: 750
   },
   floor: {
      image: "Assets/img/floor.png",
      size: 64
   },
   grass: {
      image: "Assets/img/grass.png",
      size: 64
   },
   objects: {
      trees:{
         images:{
            1: "Assets/img/objects/object_8.png",
            2: "Assets/img/objects/object_9.png",
            3: "Assets/img/objects/object_10.png"
         },
         width:{
            1: 116,
            2: 282,
            3: 282
         },
         height:{
            1: 44,
            2: 301,
            3: 275
         }
      },
      bushes: {
         images: {
            1: "Assets/img/objects/object_1.png",
            2: "Assets/img/objects/object_2.png",
            3: "Assets/img/objects/object_3.png",
            4: "Assets/img/objects/object_4.png"
         },
         width: {
            1: 133,
            2: 133,
            3: 73,
            4: 73
         },
         height:{
            1: 65,
            2: 65,
            3: 47,
            4: 47
         }
      },
      mushrooms: {
         images:{
            1: "Assets/img/objects/object_5.png",
            2: "Assets/img/objects/object_6.png"
         },
         width: {
            1: 49,
            2: 49
         },
         height:{
            1: 41,
            2: 41
         }
      },
      rocks:{
         images: {
            1: "Assets/img/objects/object_7.png"
         },
         width: {
            1: 90
         },
         height: {
            1: 54
         }
      },
      crates:{
         images: {
            1: "Assets/img/objects/object_11.png"
         },
         width: {
            1: 77
         },
         height: {
            1: 77
         }
      }
   },
   character: {
      width: 128,
      height: 88,
      walk: {
         1: "Assets/img/character/walk_1.png",
         2: "Assets/img/character/walk_2.png",
         3: "Assets/img/character/walk_3.png",
         4: "Assets/img/character/walk_4.png",
         5: "Assets/img/character/walk_5.png",
         6: "Assets/img/character/walk_6.png",
         7: "Assets/img/character/walk_7.png",
         8: "Assets/img/character/walk_8.png",
         9: "Assets/img/character/walk_9.png",
         10: "Assets/img/character/walk_10.png"
      },
      run: {
         1: "Assets/img/character/run_1.png",
         2: "Assets/img/character/run_2.png",
         3: "Assets/img/character/run_3.png",
         4: "Assets/img/character/run_4.png",
         5: "Assets/img/character/run_5.png",
         6: "Assets/img/character/run_6.png",
         7: "Assets/img/character/run_7.png",
         8: "Assets/img/character/run_8.png"
      },
      idle: {
         1: "Assets/img/character/idle_1.png",
         2: "Assets/img/character/idle_2.png",
         3: "Assets/img/character/idle_3.png",
         4: "Assets/img/character/idle_4.png",
         5: "Assets/img/character/idle_5.png",
         6: "Assets/img/character/idle_6.png",
         7: "Assets/img/character/idle_7.png",
         8: "Assets/img/character/idle_8.png",
         9: "Assets/img/character/idle_9.png",
         10: "Assets/img/character/idle_10.png"
      },
      jump: {
         1: "Assets/img/character/jump_1.png",
         2: "Assets/img/character/jump_2.png",
         3: "Assets/img/character/jump_3.png",
         4: "Assets/img/character/jump_4.png",
         5: "Assets/img/character/jump_5.png",
         6: "Assets/img/character/jump_6.png",
         7: "Assets/img/character/jump_7.png",
         8: "Assets/img/character/jump_8.png",
         9: "Assets/img/character/jump_9.png",
         10: "Assets/img/character/jump_10.png",
         11: "Assets/img/character/jump_11.png",
         12: "Assets/img/character/jump_12.png"
      }
   },
   enemies: {
      cats:{
         width: 128,
         height: 88,
         walk:{
            1: "Assets/img/enemies/cat/walk_1.png",
            2: "Assets/img/enemies/cat/walk_2.png",
            3: "Assets/img/enemies/cat/walk_3.png",
            4: "Assets/img/enemies/cat/walk_4.png",
            5: "Assets/img/enemies/cat/walk_5.png",
            6: "Assets/img/enemies/cat/walk_6.png",
            7: "Assets/img/enemies/cat/walk_7.png",
            8: "Assets/img/enemies/cat/walk_8.png",
            9: "Assets/img/enemies/cat/walk_9.png",
            10: "Assets/img/enemies/cat/walk_10.png"
         }
      }
   }
};
