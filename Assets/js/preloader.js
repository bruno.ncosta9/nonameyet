"use strict";

function preloadImage(url){
   new Image().src = url;
}

preloadImage(settings.sky.image);
preloadImage(settings.floor.image);
preloadImage(settings.grass.image);

Object.keys(settings.objects.trees.images).forEach(function(key) {
   preloadImage(settings.objects.trees.images[key]);
});

Object.keys(settings.objects.bushes.images).forEach(function(key) {
   preloadImage(settings.objects.bushes.images[key]);
});

Object.keys(settings.objects.mushrooms.images).forEach(function(key) {
   preloadImage(settings.objects.mushrooms.images[key]);
});

Object.keys(settings.objects.rocks.images).forEach(function(key) {
   preloadImage(settings.objects.rocks.images[key]);
});

Object.keys(settings.character.idle).forEach(function(key) {
   preloadImage(settings.character.idle[key]);
});

Object.keys(settings.character.idle).forEach(function(key) {
   preloadImage(settings.character.idle[key]);
});

Object.keys(settings.character.walk).forEach(function(key) {
   preloadImage(settings.character.walk[key]);
});

Object.keys(settings.character.run).forEach(function(key) {
   preloadImage(settings.character.run[key]);
});

Object.keys(settings.character.jump).forEach(function(key) {
   preloadImage(settings.character.jump[key]);
});

Object.keys(settings.enemies.cats.walk).forEach(function(key) {
   preloadImage(settings.enemies.cats.walk[key]);
});
