"use strict";

class Events
{

   _start;

   constructor($game, engine, animation, movement){
      this.$game     = $game;
      this.engine    = engine;
      this.animation = animation;
      this.movement  = movement;
   }

   init(){
      this._start = false;
   }

   clicks(character){

      window.addEventListener("keydown", (key) => {

         if(key.keyCode === 87 || key.keyCode === 38 || key.keyCode === 32) {

            if(this.engine.ground() === character.position.y){
               this.animation.jump(character);
               this.animation.doubleJump(false);
            } else if(this.animation.doubleJump() === false){
               this.animation.jump(character);
               this.animation.doubleJump(true);
            }

         }

         if(key.keyCode === 68 || key.keyCode === 39) {
            this.movement.direction("right");
            this.animation.walk(character);
         }

         if(key.keyCode === 65 || key.keyCode === 37){
            this.movement.direction("left");
            this.animation.walk(character);
         }

      });

      window.addEventListener("keyup", (key) => {

         if(key.keyCode === 68 || key.keyCode === 65 || key.keyCode === 39 || key.keyCode === 37) {
            this.movement.acceleration(0);
            this.animation.flag("_character", "walk", false);
            this.animation.flag("_character", "run", false);
         }

      });
   }

   actions(character){

      character = this.animation.idle(character);

      if(this.animation.flag("_character", "walk")  === true) {
         character = this.animation.walk(character);
         character = this.movement.walk(character)

         if(this.movement.acceleration() === true){
            this.animation.flag("_character", "walk", false);
            this.animation.flag("_character", "run", true);
         };

      }

      if(this.animation.flag("_character", "run") === true) {
         character = this.animation.run(character);
         character = this.movement.run(character)
      }

      if(this.animation.flag("_character", "jump") === true){
         character = this.animation.jump(character);
         character = this.movement.jump(character);
      }

      return character;

   }

   start(character){
      if(character.position.x >= (this.$game.width/4)){
         character.position.x = this.$game.width/4;
         this.movement.start(true);
      } else {
         this.movement.start(false);
      }
      return character;
   }

}
