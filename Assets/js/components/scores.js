class Scores
{

   _scores;

   constructor(animation){
      this.animation = animation;
   }

   init(){
      this._scores = {
         value: 1000,
         x: 20,
         y: 40,
         style: {
            color: "#000",
            font: "40px serif"
         }
      }
   }

   get(){
      return this._scores;
   }

   getValue(){
      return this._scores.value;
   }

   negative(){
      if(this._scores.value <= 0){
         this._scores.value = 0;
      }
   }

   set(){

      if(this.animation.flag("_character", "walk") === true){
         this._scores.value++;
      } else if(this.animation.flag("_character", "run") === true){
         this._scores.value += 2;
      } else if(this.animation.flag("_character", "jump") === true){
         this._scores.value += 3;
      } else {
         this._scores.value--;
      }

      this._scores.value--;

      this.negative();
   }

}
