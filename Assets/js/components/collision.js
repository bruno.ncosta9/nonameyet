class Collision
{

   _collision;
   _data = new Array();

   constructor(engine, animation, movement){
      this.engine    = engine;

      this.animation = animation;
      this.movement  = movement;
   }

   init(){
      this._collision = {
         onTop: {
            flag: false
         }
      };
   }

   data(){
      for (let item of arguments) {
         this._data = this._data.concat(item);
      }
   }

   flag(stand, set = null){
      return set != null ? this._collision[stand].flag = set : this._collision[stand].flag;
   }

   onSides(character, item){
      if(
         character.position.x >= item.position.x-item.position.width &&
         character.position.x <= item.position.x+item.position.width  &&
         character.position.y > item.position.y-item.position.height
      ){
         return true;
      }
      return false;
   }

   onTop(character, item){
      if(
         character.position.x >= item.position.x-item.position.width &&
         character.position.x <= item.position.x+item.position.width &&
         character.position.y <= item.position.y-item.position.height
      ){
         return true;
      }
      return false;
   }

   fromLeft(character, item){
      if(character.position.x < item.position.x){
         this.animation.flag("_character", "run", false);
         this.movement.acceleration(0);
         character.position.x = item.position.x-item.position.width;
      }
      return character
   }

   fromRight(character, item){
      if(character.position.x > item.position.x){
         this.animation.flag("_character", "run", false);
         this.movement.acceleration(0);
         character.position.x = item.position.x+item.position.width;
      }
      return character
   }

   fromTop(item){
      if(this.flag("onTop") === false){
         this.engine.ground(item.position.y-item.position.height);
      }
      this.flag("onTop", true);
   }

   reset(){
      if(this.flag("onTop") === false){
         this.engine.ground("default");
      }
      this.flag("onTop", false);
   }

   detection(character){

      this.reset();

      this._data.forEach((item) => {

         if(this.onTop(character, item) === true){
            this.fromTop(item);
         }

         if(this.onSides(character, item) === true){

            if(item.type == "enemy"){
               character.status = "death";
            }

            character = this.fromLeft(character, item);
            character = this.fromRight(character, item);
         }

      });

      return character;
   }

}
