"use strict";

class Objects
{

   _trees        = new Array();
   _bushes       = new Array();
   _mushrooms    = new Array();
   _rocks        = new Array();
   _crates       = new Array();
   _positionFix = 10;

   constructor($game, settings){
      this.$game    = $game;
      this.settings = settings;
   }

   trees(){

      let positionX  = 0;
      const distance = 120;
      const statics  = Math.round(this.$game.width/distance);
      const rows     = Object.keys(this.settings.trees.images).length;

      for(let i = 1; i <= statics; i++){

         let object = Math.floor((Math.random() * rows) + 1);

         const image = new Image();
         image.src = this.settings.trees.images[object];

         this._trees[i] = {
            image : image,
            position : {
               x : positionX,
               y : this.$game.height - (2*64) - this.settings.trees.height[object] + this._positionFix,
               width : this.settings.trees.width[object],
               height: this.settings.trees.height[object]
            },
            type : "object"
         };
         positionX += distance;
      }

   }

   bushes(){

      let positionX  = 0;
      const distance = 220;
      const statics  = Math.round(this.$game.width/distance);
      const rows     = Object.keys(this.settings.bushes.images).length

      for(let i = 1; i <= statics; i++){

         let object = Math.floor((Math.random() * rows) + 1);

         const image = new Image();
         image.src = this.settings.bushes.images[object];

         this._bushes[i] = {
            image : image,
            position : {
               x : positionX,
               y : this.$game.height - (2*64) - this.settings.bushes.height[object] + this._positionFix,
               width : this.settings.bushes.width[object],
               height: this.settings.bushes.height[object]
            },
            type : "object"
         };
         positionX += distance;
      }

   }

   mushrooms(){

      let positionX  = 460;
      const distance = 460;
      const statics  = Math.round(this.$game.width/distance);
      const rows     = Object.keys(this.settings.mushrooms.images).length

      for(let i = 1; i <= statics; i++){

         let object = Math.floor((Math.random() * rows) + 1);

         const image = new Image();
         image.src = this.settings.mushrooms.images[object];

         this._mushrooms[i] = {
            image : image,
            position : {
               x : positionX,
               y : this.$game.height - (2*64) - this.settings.mushrooms.height[object] + this._positionFix,
               width : this.settings.mushrooms.width[object],
               height: this.settings.mushrooms.height[object]
            },
            type : "object"
         };
         positionX += distance;
      }

   }

   rocks(){

      let positionX  = 620;
      const distance = 620;
      const statics  = Math.round(this.$game.width/distance);
      const rows     = Object.keys(this.settings.rocks.images).length

      for(let i = 1; i <= statics; i++){

         let object = Math.floor((Math.random() * rows) + 1);

         const image = new Image();
         image.src = this.settings.rocks.images[object];

         this._rocks[i] = {
            image : image,
            position : {
               x : positionX,
               y : this.$game.height - (2*64) - this.settings.rocks.height[object] + this._positionFix,
               width : this.settings.rocks.width[object],
               height: this.settings.rocks.height[object]
            },
            type : "object"
         };
         positionX += distance;
      }

   }

   crates(){

      let positionX  = 720;
      const distance = 720;
      const statics  = Math.round(this.$game.width/distance);
      const rows     = Object.keys(this.settings.crates.images).length

      for(let i = 1; i <= statics; i++){

         let object = Math.floor((Math.random() * rows) + 1);

         const image = new Image();
         image.src = this.settings.crates.images[object];

         this._crates[i] = {
            image : image,
            position : {
               x : positionX,
               y : this.$game.height - (2*64) - this.settings.crates.height[object] + this._positionFix,
               width : this.settings.crates.width[object],
               height: this.settings.crates.height[object]
            },
            type : "object"
         };
         positionX += distance;
      }

   }

   init(){
      this.trees();
      this.bushes();
      this.mushrooms();
      this.rocks();
      this.crates();
   }

}
