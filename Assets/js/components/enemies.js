class Enemies
{

   _cats = new Array();

   constructor($game, settings){
      this.$game    = $game;
      this.settings = settings;
   }

   cats(){

      const image = new Image();

      let positionX  = 980;
      const distance = 980;
      const statics  = Math.round(this.$game.width/distance);

      for(let i = 1; i <= statics; i++){

         this._cats[i] = {
            image : image,
            position : {
               original: this.$game.height - (2*64) - this.settings.cats.height + 30,
               x : positionX,
               y : this.$game.height - (2*64) - this.settings.cats.height + 20,
               width : this.settings.cats.width,
               height: this.settings.cats.height
            },
            type : "enemy"
         };

         positionX += distance;

      }

   }

   init(){
      this.cats();
   }

}
