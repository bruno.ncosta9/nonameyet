"use strict";

class Grass
{

   _grass = new Object();

   constructor($game, settings){
      this.$game    = $game;
      this.settings = settings;
   }

   init(){
      const image = new Image();
      image.src = this.settings.image;

      this._grass = {
         image : image,
         position : {
            x : 0,
            y : 0,
            width : this.$game.width,
            height: this.settings.size
         },
         translate : {
            x: 0,
            y: this.$game.height - (2*this.settings.size),
         }
      };
   }

}
