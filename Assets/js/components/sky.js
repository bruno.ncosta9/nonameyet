"use strict";

class Sky
{

   _sky = new Object();

   constructor($game, settings){
      this.$game    = $game;
      this.settings = settings;
   }

   init(){
      const image = new Image();
      image.src = this.settings.image;

      this._sky = {
         image :image,
         position : {
            x : 0,
            y : 0,
            width : this.$game.width,
            height: this.$game.height
         },
         translate : {
            x: 0,
            y: this.$game.height-this.settings.height
         }
      }
   }

}
