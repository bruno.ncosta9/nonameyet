"use strict";

class Movement
{
   _direction;
   _acceleration;
   _start;

   constructor($game, settings, animation){
      this.$game     = $game;
      this.settings  = settings;
      this.animation = animation;
   }

   init(){
      this._direction = "right";
      this._acceleration = 0;
      this._start = false;
   }

   direction(set = null){
      return set != null ? this._direction = set : this._direction;
   }

   acceleration(set = null){
      this._acceleration++;
      this._acceleration = set != null ? set : this._acceleration;
      if(this._acceleration == 60){
         this._acceleration = 0;
         return true;
      }
      return false;
   }

   start(set = null){
      return set != null ? this._start = set : this._start;
   }

   background(object){
      if(this.direction() == "right"){
         if(this.animation.flag("_character", "walk") === true){
            this.start() === true ? object.position.x += 2 : object.position.x++;
            this.start() === true ? object.translate.x += 2 : object.translate.x++;
         }
         if(this.animation.flag("_character", "run") === true) {
            this.start() === true ? object.position.x += 4 : object.position.x++;
            this.start() === true ? object.translate.x += 4 : object.translate.x++;
         }
      }
      if(this.direction() == "left"){
         if(this.animation.flag("_character", "walk") === true){
            this.start() === true ? object.position.x -= 2 : object.position.x--;
            this.start() === true ? object.translate.x -= 2 : object.translate.x--;
         }
         if(this.animation.flag("_character", "run") === true) {
            this.start() === true ? object.position.x -= 4 : object.position.x--;
            this.start() === true ? object.translate.x -= 4 : object.translate.x--;
         }
      }
      return object;
   }

   statics(object){
      if(this.direction() == "right"){
         if(this.animation.flag("_character", "walk") === true){
            this.start() === true ? object.position.x -= 2 : object.position.x--;
         }
         if(this.animation.flag("_character", "run") === true) {
            this.start() === true ? object.position.x -= 4 : object.position.x--;
         }
      }
      if(this.direction() == "left"){
         if(this.animation.flag("_character", "walk") === true){
            this.start() === true ? object.position.x += 2 : object.position.x++;
         }
         if(this.animation.flag("_character", "run") === true) {
            this.start() === true ? object.position.x += 4 : object.position.x++;
         }
      }
      return object;
   }

   walk(object){
      if(this.start() === true){
         return object;
      }
      if(this.direction() == "right"){
         object.position.x += 2;
      }
      if(this.direction() == "left"){
         object.position.x -= 2;
      }
      return object;
   }

   run(object){
      if(this.start() === true){
         return object;
      }
      if(this.direction() == "right"){
         object.position.x += 4;
      }
      if(this.direction() == "left"){
         object.position.x -= 4;
      }
      return object;
   }

   jump(object){
      object.position.y -= 4;
      return object;
   }

   enemies(object){
      if(this.direction() == "right" && this.animation.flag("_character", "walk") === true){
         object.position.x--;
      }
      if(this.direction() == "left" && this.animation.flag("_character", "walk") === true){
         object.position.x++;
      }
      return object;
   }

}
