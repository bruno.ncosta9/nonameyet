"use strict";

class Animation
{

   _character = new Object();
   _enemies = new Object();

   constructor($game, settings){
      this.$game    = $game;
      this.settings = settings;
   }

   init(){
      this._character = {
         idle : {
            counter : 0,
            image : 1,
            imagesLength : Object.keys(this.settings.character.idle).length
         },
         walk : {
            flag: false,
            counter : 0,
            image : 1,
            imagesLength : Object.keys(this.settings.character.run).length
         },
         run : {
            flag: false,
            counter : 0,
            image : 1,
            imagesLength : Object.keys(this.settings.character.run).length
         },
         jump : {
            doubleJump: false,
            flag: false,
            counter : 0,
            image : 1,
            imagesLength : Object.keys(this.settings.character.jump).length,
         }
      }
      this._enemies = {
         cats : {
            flag: false,
            counter : 0,
            image : 1,
            imagesLength : Object.keys(this.settings.enemies.cats.walk).length
         }
      }
   }

   flag(type, stand, set = null){
      return set != null ? this[type][stand].flag = set : this[type][stand].flag;
   }

   idle(object){

      if(this._character.idle.counter == 10){
         this._character.idle.image = this._character.idle.image == this._character.idle.imagesLength ? 0 : this._character.idle.image;
         this._character.idle.image ++;
         this._character.idle.counter = 0;
      }

      object.image.src = this.settings.character.idle[this._character.idle.image];

      this._character.idle.counter++;

      return object;
   }

   walk(object){
      this.flag("_character", "walk", true);

      if(this._character.walk.counter == 10){
         this._character.walk.image = this._character.walk.image == this._character.walk.imagesLength ? 0 : this._character.walk.image;
         this._character.walk.image ++;
         this._character.walk.counter = 0;
      }

      object.image.src = this.settings.character.walk[this._character.walk.image];

      this._character.walk.counter++;

      return object;
   }

   run(object){
      this.flag("_character", "run", true);

      if(this._character.run.counter == 10){
         this._character.run.image = this._character.run.image == this._character.run.imagesLength ? 0 : this._character.run.image;
         this._character.run.image ++;
         this._character.run.counter = 0;
      }

      object.image.src = this.settings.character.run[this._character.run.image];

      this._character.run.counter++;

      return object;
   }

   doubleJump(set = null){
      return set != null ? this._character.jump.doubleJump = set : this._character.jump.doubleJump;
   }

   jump(object){
      this.flag("_character", "jump", true);

      if(this._character.jump.counter == 3){
         this._character.jump.image++;
         this._character.jump.counter = 0;
      }

      object.image.src = this.settings.character.jump[this._character.jump.image];

      if(this._character.jump.image == this._character.jump.imagesLength){
         this._character.jump.image = 1;
         this.flag("_character", "jump", false);
      }

      this._character.jump.counter++;

      return object;
   }

   enemies(object){

      if(this._enemies.cats.counter == 10){
         this._enemies.cats.image = this._enemies.cats.image == this._enemies.cats.imagesLength ? 0 : this._character.walk.image;
         this._enemies.cats.image ++;
         this._enemies.cats.counter = 0;
      }

      object.image.src = this.settings.enemies.cats.walk[this._enemies.cats.image];

      this._enemies.cats.counter++;

      return object;
   }

}
