"use strict";

class Floor
{

   _floor = new Object();

   constructor($game, settings){
      this.$game    = $game;
      this.settings = settings;
   }

   init(){
      const image = new Image();
      image.src = this.settings.image;

      this._floor = {
         image : image,
         position : {
            x : 0,
            y : 0,
            width : this.$game.width,
            height: this.settings.size + 10
         },
         translate : {
            x: 0,
            y: this.$game.height - this.settings.size
         }
      };
   }

}
