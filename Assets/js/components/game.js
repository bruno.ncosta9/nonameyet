"use strict";

class Game
{

   constructor(settings){
      this.settings  = settings;
   }

   initCanvas(){
      this.canvas  = new Canvas();
      this.canvas.init();

      this.$game   = this.canvas.$game;
      this.context = this.canvas.context;
   }

   initBackground(){
      this.sky = new Sky(this.$game, this.settings["sky"]);
      this.sky.init();

      this.floor = new Floor(this.$game, this.settings["floor"]);
      this.floor.init();

      this.grass = new Grass(this.$game, this.settings["grass"]);
      this.grass.init();
   }

   initStatics(){
      this.objects = new Objects(this.$game, this.settings["objects"]);
      this.objects.init();
   }

   initCharacter(){
      this.character = new Character(this.$game, this.settings["character"]);
      this.character.init();
   }

   initEnemies(){
      this.enemies = new Enemies(this.$game, this.settings["enemies"]);
      this.enemies.init();
   }

   initEngine(){
      this.engine = new Engine(this.settings);
      this.engine.init();

      this.engine.groundDefault(this.$game.height - (2*64) - this.character._character.position.height + 20);
      this.engine.ground(this.$game.height - (2*64) - this.character._character.position.height + 20);
   }

   initEvents(){
      this.animation = new Animation(this.$game, this.settings);
      this.animation.init();

      this.movement = new Movement(this.$game, this.settings, this.animation);
      this.movement.init();

      this.events = new Events(this.$game, this.engine, this.animation, this.movement);
      this.events.init();

      this.collision = new Collision(this.engine, this.animation, this.movement);
      this.collision.init();
   }

   initScores(){
      this.scores = new Scores(this.animation);
      this.scores.init();
   }

   init(){
      this.initCanvas();

      this.initBackground();
      this.initStatics();
      this.initCharacter();
      this.initEnemies();

      this.initEngine();

      this.initEvents();
   }

   clear(){
      this.context.clearRect(0, 0, this.$game.width, this.$game.height);
   }

   drawText(object){
      this.context.save();
      this.context.font = object.style.font;
      this.context.fillStyle = object.style.color;
      this.context.fillText(object.value, object.x, object.y);
      this.context.restore();
   }

   drawEnemies(object){

      object.forEach((item) => {

         item = this.animation.enemies(item);
         item = this.movement.enemies(item);

         this.context.save();

         this.context.drawImage(
            item.image,
            item.position.x,
            item.position.y,
            item.position.width,
            item.position.height
         );

         this.context.restore();

      });

   }

   drawStatics(object){

      object.forEach((item) => {

         item = this.movement.statics(item);

         this.context.save();

         this.context.drawImage(
            item.image,
            item.position.x,
            item.position.y,
            item.position.width,
            item.position.height
         );

         this.context.restore();

      });

   }

   drawPattern(object){
      object = this.movement.background(object);

      this.context.fillStyle = this.context.createPattern(object.image, "repeat");

      this.context.save();
      this.context.translate(
         -object.translate.x,
         object.translate.y
      );

      this.context.fillRect(
         object.position.x,
         object.position.y,
         object.position.width,
         object.position.height
      );

      this.context.restore();
   }

   draw(){

      this.reset();

      this.character._character = this.events.actions(this.character._character);

      // this.character._character = this.events.start(this.character._character);

      this.collision.data(
         //this.enemies._cats,
         this.objects._crates
      );
      this.character._character = this.collision.detection(this.character._character);

      if(this.character._character.status == "death" || this.scores.getValue() === 0){
         this.restart();
      }

      this.character._character.position.y = this.engine.gravity(this.character._character.position.y, this.animation.flag("_character", "jump"));

      this.clear();

      this.drawPattern(this.sky._sky);
      this.drawPattern(this.floor._floor);
      this.drawPattern(this.grass._grass);

      this.drawStatics(this.objects._trees);
      this.drawStatics(this.objects._bushes);
      this.drawStatics(this.objects._mushrooms);
      this.drawStatics(this.objects._rocks);

      this.drawStatics(this.objects._crates);

      //this.drawEnemies(this.enemies._cats);

      this.scores.set();
      this.drawText(this.scores.get());

      this.context.save();

      if(this.movement.direction() === "left"){
         this.context.scale(-1, 1);
      }

      this.context.drawImage(
         this.character._character.image,
         this.movement.direction() === "left" ? -this.character._character.position.x-this.character._character.position.width : this.character._character.position.x,
         this.character._character.position.y,
         this.character._character.position.width,
         this.character._character.position.height
      );

      this.context.restore();

      requestAnimationFrame(this.draw.bind(this));
   }

   start(){
      this.init()
      this.initScores();
      this.events.clicks(this.character._character);
      this.draw();
   }

   restart(){
      this.init();
      this.initScores();
      this.events.clicks(this.character._character);
   }

   reset(){
      if(this.character._character.position.x >= this.$game.width || this.character._character.position.x < 0){
         this.restart();
      }
   }

}
