"use strict";

class Character
{

   _character = new Object();

   constructor($game, settings){
      this.$game     = $game;
      this.settings  = settings;
   }

   position(axis, set = null){
      return set != null ? this._character.position[axis] = set : this._character.position[axis];
   }

   size(type, set = null){
      return set != null ? this._character.position[type] = set : this._character.position[type];
   }

   status(set = null){
      return set != null ? this._character.status = set : this._character.status;
   }

   init(){
      const image = new Image();

      this._character = {
         image : image,
         position : {
            original: this.$game.height - (2*64) - this.settings.height + 20,
            x : 0,
            y : this.$game.height - (2*64) - this.settings.height + 20,
            width : this.settings.width,
            height: this.settings.height
         },
         status: "alive"
      };
   }

}
