# NoNameYet "Dino"

First time exploring GameDev using **Javascript** and **Canvas API**.
Not fishished to play.

## Live

* [brunoncosta.com](http://brunoncosta.com/GAME/nonameyet/index.html)

## Built With

* HTML
* CSS
* JS
* [Canvas API](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API)

## Sprites and Images from

* [Game Art 2D](https://www.gameart2d.com/freebies.html)

## Authors
* **Bruno Costa**
